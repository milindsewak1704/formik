import React from 'react';
import PropTypes from 'prop-types';
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";

const CheckboxField = ({
    label,
    value,
    id,
    name,
    onChange,
    color
}) => {

    const [checked, setChecked] = React.useState(value);

    const handleChange = (event) => {
        setChecked(event.target.checked);
        onChange(event.target.checked)
    };

    return (
        <FormControlLabel
            control={<Checkbox checked={checked} name={name} id={id} onChange={handleChange} color={color} />}
            label={label}
        />
    )
}

CheckboxField.defaultProps = {
    color: "primary"
}

CheckboxField.propTypes = {
    label: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.string,
    id: PropTypes.string,
    onChange: PropTypes.func,
    color: PropTypes.string,
}

export default CheckboxField;
