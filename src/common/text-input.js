import PropTypes from "prop-types";
import React from 'react';
import FormHelperText from '@material-ui/core/FormHelperText';
import TextField from '@material-ui/core/TextField';

const TextInput = ({
    label,
    id,
    variant,
    type,
    helperText,
    value,
    error,
    multiline,
    rowsMax,
    customstyle,
    disabled,
    required,
    onChange,
    inputProps,
    autoComplete,
    characterLimit
}) => {

    const handleChange = (event) => {
        onChange(event);
    }

    return (
        <>
            <TextField
                id={id}
                label={label}
                variant={variant}
                type={type}
                value={value}
                error={error}
                multiline={multiline}
                rowsMax={rowsMax}
                onChange={handleChange}
                disabled={disabled}
                required={required}
                InputProps={inputProps}

            />
            {
                (10 && value?.length > 5) &&
                <FormHelperText className="textarea-count">{value?.length + "/" + 10}</FormHelperText>
            }
            {helperText && <FormHelperText error={error}>{helperText}</FormHelperText>}
        </>
    )
}

TextInput.propTypes = {
    customstyle: PropTypes.string,
    disabled: PropTypes.bool,
    error: PropTypes.bool,
    helperText: PropTypes.string,
    id: PropTypes.string,
    label: PropTypes.string,
    multiline: PropTypes.any,
    onChange: PropTypes.func,
    required: PropTypes.bool,
    rowsMax: PropTypes.any,
    type: PropTypes.any,
    value: PropTypes.any,
    variant: PropTypes.string,
    inputProps: PropTypes.any,
    autoComplete: PropTypes.any,
    characterLimit: PropTypes.number,
}

TextInput.defaultProps = {
    customstyle: "",
    multiline: false
}

export default TextInput;
