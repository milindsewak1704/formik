import React from 'react';
import PropTypes from 'prop-types';
import { TextField, MenuItem, FormHelperText } from '@material-ui/core';

const Select = ({
    label,
    id,
    variant,
    helperText,
    error,
    value,
    data,
    customstyle,
    disabled,
    required,
    onChange,
    name
}) => {

    return (
        <>
            <TextField
                select
                id={id}
                name={name}
                label={label}
                variant={variant}
                error={error}
                value={value}
                onChange={onChange}
                disabled={disabled}
                required={required}
            // style={{ paddingRight: '275px' }}
            >
                {
                    data.map((item, key) => {
                        return (
                            <MenuItem key={key} value={item.value} disabled={item.disabled}>
                                {item.label}
                            </MenuItem>
                        )
                    })
                }
            </TextField>
            {helperText && <FormHelperText error={error}>{helperText}</FormHelperText>}
        </>
    )
}

Select.defaultProps = {
    customstyle: ""
}

Select.propTypes = {
    label: PropTypes.string,
    id: PropTypes.string,
    variant: PropTypes.string,
    helperText: PropTypes.string,
    error: PropTypes.bool,
    value: PropTypes.object.isRequired,
    data: PropTypes.array.isRequired,
    customstyle: PropTypes.string,
    disabled: PropTypes.bool,
    required: PropTypes.bool,
    onChange: PropTypes.func.isRequired,
    name: PropTypes.string,
}

export default Select;
