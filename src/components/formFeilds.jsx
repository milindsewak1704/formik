// import './App.css';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import TextInput from '../common/text-input';
import Select from '../common/select';
import Button from '@material-ui/core/Button';
import CheckboxField from '../common/checkbox-field';
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(1),
        textAlign: "center",
        color: theme.palette.text.secondary
    }
}));

const FormFeilds = ({ value }) => {
    const classes = useStyles();

    const REGEX_MOBILE = /^[6-9]\d{9}$/

    const formik = useFormik({
        initialValues: {
            mobileNo: '',
            country: '',
            checkbox: true
        },
        validationSchema: Yup.object({
            mobileNo: Yup.string().required('Enter mobile number').matches(REGEX_MOBILE, 'Invalid mobile number'),
        }),
        onSubmit: (values) => {
        }
    });
    return (
        <div className="App">
            <form onSubmit={formik.handleSubmit}>

                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6} md={3}>
                        <Paper className={classes.paper}>

                            <TextInput
                                label="Mobile number"
                                id="mobileNo"
                                name="mobileNo"
                                variant="filled"
                                type="text"
                                value={formik.values.mobileNo}
                                onChange={(event) => {
                                    formik.handleChange(event)
                                }}
                                error={formik.touched.mobileNo && Boolean(formik.errors.mobileNo)}
                                helperText={formik.touched.mobileNo && formik.errors.mobileNo}
                                customstyle="field-wrap"
                                required={true}
                            />
                        </Paper>
                        <Grid item xs={12} sm={6} md={3}>
                            <Paper className={classes.paper}>

                                <Select
                                    label="Select Country *"
                                    id="country"
                                    name="country"
                                    variant="filled"
                                    data={[
                                        {
                                            value: true,
                                            label: 'Yes',
                                            disabled: false
                                        },
                                        {
                                            value: false,
                                            label: 'No',
                                            disabled: false
                                        }
                                    ]}
                                    value={formik.values.country}
                                    error={formik.touched.country && Boolean(formik.errors.country)}
                                    helperText={formik.touched.country && formik.errors.country}
                                // disabled={!proposedData.gate?.manualMode}
                                // onChange={(event) => handleIsPolicyExpired(event)}


                                />
                            </Paper>
                        </Grid>
                        <Grid item xs={12} sm={6} md={3}>
                            <Paper className={classes.paper}>

                                <CheckboxField
                                    id="checkbox"
                                    label="Is vehicle registration address is same as communication address?"
                                    name="checkbox"
                                    value={formik.values.checkbox}
                                // onChange={(event) => { formik.setFieldValue('communicationAddressSameAsRegistration', event) }}
                                />

                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>




                <Button variant="contained" className="btn btn-contained btn-primary btn-submit"
                    size="large" type="submit">Submit</Button>

            </form >
        </div >
    );
}

export default FormFeilds;
