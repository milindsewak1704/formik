import logo from './logo.svg';
import './App.css';
import FormFeilds from './components/formFeilds';

function App() {
  return (
    <div className="App">
      <FormFeilds />
    </div>
  );
}

export default App;
